<?php


namespace WAPDC\Util;


use Symfony\Component\Yaml\Parser;

/**
 * Class Environment
 *
 * Provides utilities for manipulating environment variables.
 */
class Environment {

  /**
   * Loads PHP $_ENV environment variables from a YAML file so that configuration information can be injected.
   *
   * @param $filename
   *   File to load.
   */
  public static function loadFromYml($filename) {
    if (file_exists($filename)) {
      $parser = new Parser();
      $env = $parser->parse(file_get_contents($filename));
      if ($env) foreach($env as $key => $value) {
        $_ENV[$key] = $value;
      }
    }
  }


}
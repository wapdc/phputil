<?php


namespace WAPDC\Util;

/**
 * Class Revisions
 *
 * Class for managing deployment revisions for PHP Projects.
 */
abstract class RevisionManager {

  protected $script_directory;

  public $exit_on_fail = TRUE;

  public function __construct($dir) {
    $this->script_directory = $dir;
  }

  /**
   * Execute the rev script for a version.
   * @param $version
   * @return bool
   */
  public function runRevScripts() {
    $current_version = $this->getVersion();
    echo "Executing revision scripts from ". $this->script_directory . "\n";
    echo "Current Version: $current_version\n\n";
    $without_error = TRUE;

    // Find all the files that match rev.*.*.* and create indexed array by rev id of the script name
    $dir = $this->script_directory . "/rev.*.*.*";
    foreach (glob($dir) as $filename) {
      list($prefix, $version, $comment, $ext ) = explode('.', $filename, 4);
      if($current_version < $version){

        // call script
        echo "Executing $filename ....\n\n";
        $success = $this->runScript($filename);
        // if successful update version #
        if($success) {
         $this->updateVersion($version);
         $current_version = $version;
         echo "\n";
        } else{
          echo "Error in $filename\n";
          $without_error = FALSE;
          if ($this->exit_on_fail && !$without_error) {
            throw new \Exception("Error in $filename", 500);
          }
        }
      }
    }

    return $without_error;
  }

  /**
   * Return the current version of the application.
   * @return string
   */
  abstract protected function getVersion();


  /**
   * Update the revision number on successful completion.
   *
   * @param string $version
   *   $the new version number
   */
  abstract protected function updateVersion($version);

  /**
   * Executes shell scripts using php
   * TODO: find a way to get $exit_code on Windows
   * This code works as is on Mac OS and fails on Windows.
   *
   * In Windows a separate terminal fires for every .sh file found and ran.
   * $exit_code is not set properly causing the tests to fail consistently.
   * use of exe(), shell_exe(), system() and etc. return the same results on Windows.
   *
   * @param $file
   * @return bool
   */
  protected function runScript($file) {
   $exit_code = 0;
   passthru($file, $exit_code);
   return !$exit_code;
  }
}
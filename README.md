# PHP Utilities
This will house general PHP utilities used in the projects for the Washington 
Public Disclosure Commission. 

Note that this should not contain any PDC model specific code, but only general
utilities that are used by the team. 
<?php


namespace Tests\WAPDC;


use PHPUnit\Framework\TestCase;
use Tests\WAPDC\Mock\TestRevisionManager;

class RevisionManagerTest extends TestCase {

  public function testRevScripts() {
    $rev_dir = dirname(__DIR__) . '/RevScripts';
    $revisions = new TestRevisionManager($rev_dir);
    $revisions->exit_on_fail = FALSE;

    // Run first version and assume failure.
    $success  = $revisions->runRevScripts();
    $this->assertFalse($success, 'Succeeded');

    // Run second version and assume failure
    $revisions->updateVersion('2018-01-000');
    $success = $revisions->runRevScripts();
    $this->assertFalse($success);

    // Pretend that we have already run 2018-01-001 successfully
    $revisions->updateVersion('2018-01-001');

    // now run the scripts
    $success = $revisions->runRevScripts();
    $this->assertTrue($success);
    $this->assertEquals('2018-01-002', $revisions->getVersion());

  }
}
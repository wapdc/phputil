<?php


namespace Tests\WAPDC\Mock;


use WAPDC\Util\RevisionManager;

class TestRevisionManager extends RevisionManager {
  /**
   * @var string Current version
   */
  public $version = '';

  public function updateVersion($version) {
    $this->version = $version;
  }

  public function getVersion() {
    return $this->version;
  }


}
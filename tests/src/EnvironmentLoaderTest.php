<?php


namespace Tests\WAPDC;


use PHPUnit\Framework\TestCase;
use WAPDC\Util\Environment;

class EnvironmentLoaderTest extends TestCase {

  public function testEnvironmentLoader() {
    $file = dirname(__DIR__) .'/data/environment.yml';
    Environment::loadFromYml($file);
    $this->assertEquals('some_value', $_ENV['TEST_ENV_VARIABLE']);
  }
}